# K-Means Clustering

Created on December 5th, 2018

## Purpose and Disclaimer

All of the scripts / code present in this repository is solely for the purpose of further understanding the k-means clustering algorithm in different languages. Most code and script contained in this repository are based off of the work of others and I do not claim ownership of any content which may be similar to other open source work. 

Any code that is seen by the public is open source and may be used for private purposes. 