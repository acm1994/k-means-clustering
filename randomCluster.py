# Aaron Christopher Montano
# 12 / 05 / 2018
# K-Means Clustering: Sklearn.
# This script is intended to perform a k-means clustering algorithm on a set of
# random data in two dimensions.

# Import required modules.
import pandas as pd
import matplotlib.pylab as plt
import seaborn as sns
import random as rd
from sklearn.cluster import KMeans

# Retrieve user input.
# k - total number of clusters.
# n - total number of data points.
k = 0
n = 0
try:
    n = int(input("Enter the total number of desired data points:  "))
    k = int(input("Enter the total number of desired clusters:  "))
except:
    print("Error: Please enter a valid (numerical) input for n or k.  ")


# Generate a set of random variables, x and y.
x = []
y = []
for i in range(0, abs(n)):
    x.append(rd.uniform(0, 1000))
    y.append(rd.uniform(0, 1000))

mainFrame = pd.DataFrame({"x":x, "y":y})

# Fit the model.
kmeans = KMeans(n_clusters = k)
kmeans.fit(mainFrame)
labels = kmeans.predict(mainFrame)
centroids = kmeans.cluster_centers_

# Plot the model.
sns.set_style("darkgrid")
colmap = {1:'r', 2:'g', 3:'b', 4:'k'}
fig = plt.figure(figsize = (10, 10))
colors = list(map(lambda x: colmap[x+1], labels))
plt.scatter(mainFrame["x"], mainFrame["y"], color = colors, alpha = 0.8, edgecolor = 'k')
plt.xlim(-100, 1100)
plt.ylim(-100, 1100)
plt.show()
